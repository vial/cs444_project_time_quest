/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains the logic required to enable the gameObject
  work as teleportation bomb, the script attached to the GameObject will require
  a collider
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBomb : MonoBehaviour
{

    //define variables section
    public bool fired;
    public bool holdedByPlayer;
    public GameObject Player;
    private bool EnableTeleport;
    public string nameOfBomb;
    public AudioSource audioTP;
    private ParticleSystem explo;
    private Vector3 initialWorldPos;
    private GameObject refEffect;

    // Start is called before the first frame update
    void Start()
    {
        fired = false;
        holdedByPlayer = false;
        nameOfBomb = "";
        initialWorldPos = this.transform.position;
        Player = GameObject.Find("OVRPlayerController");
        //obtain the reference of the efffect of teleportation from the Player's children
        explo = Player.transform.Find("PlasmaExplosionEffect").gameObject.GetComponent<ParticleSystem>();
        explo.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);

    }

    // Update is called once per frame
    void Update()
    {
        if (fired && holdedByPlayer)
        {
            EnableTeleport = true;
        }
    }

    void changeVariable()
    {
        fired = true;
        holdedByPlayer = true;
    }
    void OnCollisionEnter(Collision collision)
    {
            if (EnableTeleport)
            {
            Invoke("Teleport", 1.0f);
            Invoke("stopEffects", 2.5f);
            EnableTeleport = false;
            fired = false;
            holdedByPlayer = false;
        }
    }

    void Teleport()
    {
        if(!holdedByPlayer)
        {
            explo.Play(true);
            audioTP.Play();
            //calculate y position by adding the current position y of the player and the bomb's
            //substract 0.08f to avoid glitch with telporting bomb
            var y_val = Player.transform.localPosition.y + (transform.localPosition.y - 0.08f);
            Player.transform.localPosition = new Vector3(transform.localPosition.x, y_val, transform.localPosition.z);
            this.transform.position = initialWorldPos;
            this.transform.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        }
    }

    void stopEffects()
    {
       explo.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }

}
