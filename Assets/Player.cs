/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic for the player
           during the game
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{


    [SerializeField]
    private int maxHealth = 100;

    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    private AudioSource dmgTaken1;

    [SerializeField]
    private AudioSource dmgTaken2;

    [SerializeField]
    private AudioSource dmgTaken3;

    [SerializeField]
    private AudioSource dmgTaken4;

    [SerializeField]
    private AudioSource dmgTaken5;

    [SerializeField]
    private AudioSource deathSound;

    [SerializeField]
    private AudioSource gameOver;

    [SerializeField]
    private AudioSource music;

    [SerializeField]
    private OVRScreenFade sFade;


    private int currentHealth;
    private List<AudioSource> soundsList = new List<AudioSource>();
    private System.Random random = new System.Random();
    private float fLevel = 0f;
    private bool gameOverDone = false;

    public event Action<float> OnHealthPctChanged = delegate { };

    // Start is called before the first frame update
    void Start()
    {
        music.Play();
        currentHealth = maxHealth;
        soundsList.Add(dmgTaken1);
        soundsList.Add(dmgTaken2);
        soundsList.Add(dmgTaken3);
        soundsList.Add(dmgTaken4);
        soundsList.Add(dmgTaken5);
    }

    // Update is called once per frame
    void Update()
    {
        //If no health then die
        if (currentHealth <= 0)
        {

            //slowly fade view and restart game
            fLevel += 0.01f;
            canvas.gameObject.SetActive(false);
            sFade.SetUIFade(fLevel);
            if(fLevel > 1.1f)
            {
                //play gameOver sound once
                if (!gameOverDone)
                {
                    gameOver.Play();
                    gameOverDone = true;
                }
                //after some little time restart game
                if (fLevel > 2f)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
            }


        }
    }

    //When hit by ennemy, take dmg + sound + animation
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Wrist_L")
        {
            if (currentHealth > 10)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                deathSound.Play();
            }

            ModifyHealth(-10);
        }
        if (other.gameObject.name == "Axe_DS_Ennemy")
        {
            if (currentHealth > 20)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                deathSound.Play();
            }

            ModifyHealth(-20);
        }

        if (other.gameObject.name == "SWORD")
        {
            if (currentHealth > 30)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                deathSound.Play();
            }

            ModifyHealth(-30);
        }

        if (other.gameObject.name.Contains("cannon ball enemy"))
        {
            if (currentHealth > 20)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                deathSound.Play();
            }

            ModifyHealth(-20);
        }
    }

    public void ModifyHealth(int amount)
    {
        currentHealth += amount;

        float currentHealthPct = (float)currentHealth / (float)maxHealth;
        OnHealthPctChanged(currentHealthPct);
    }
}
