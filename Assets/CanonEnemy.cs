/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic for enemy cannons, cannons enemies
           can be placed indivually in the game scene, or as group of cannons.
           In addition, the script manage the execution time for shooting cannonballs
           and the cannon's effect
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(MeshCollider))]

public class CanonEnemy : MonoBehaviour
{

    [Header("ShootEvery")]
    public float shootEverySec = 5.0f;

    [Header("DelayForShooting")]
    public float delayForShooting = 0.0f;


    private bool alreadyLoaded;

    public GameObject cannonBall;
    private Rigidbody cannonBallRB;
    public Transform shotPos;
    public GameObject explosion;
    public GameObject canonMesh;
    public float forwardPower;


    private ParticleSystem explo;
    private ParticleSystem smoke;
    [SerializeField]
    private AudioSource shootSound;
    private bool ballInAction;

    //Cannons enemies can be invidual placed in the scene, or they can be
    //aggregated in a GameObject as a part of a row of cannons
    public GameObject parentOfEnemyCannons;

    // Start is called before the first frame update
    void Start()
    {
        explo = this.transform.Find("Cube_001").gameObject.transform.Find("shootPosition").gameObject.transform.Find("SmallExplosionEffect").gameObject.GetComponent<ParticleSystem>();
        explo.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        smoke = this.transform.Find("SmokeEffect").gameObject.GetComponent<ParticleSystem>();
        smoke.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        canonMesh = transform.GetChild(1).gameObject;

        //generalyze script by allowing to check if cannons is a child of another
        //GameObject, thus invidual enemy cannon could be placed rather than using
        //a row of cannons
        if(this.transform.parent != null && this.transform.parent.gameObject.name.Contains("Cannon row"))
        {
            parentOfEnemyCannons = this.transform.parent.gameObject;
        }
        InvokeRepeating("shootBall", delayForShooting, shootEverySec);

    }

    void Update()
    {
        if(ballInAction)
        {
            Invoke("stopShootingEffect", 1.7f);
            ballInAction = false;
        }

    }


    private void shootBall()
    {

        //execute the shooting if enemy cannon does not form part of a row of cannons
        //or if the row of cannons has enabled the shooting
        if (parentOfEnemyCannons is null || parentOfEnemyCannons.GetComponent<ControllingCannons>().enableShooting)
        {
            shootSound.Play();
            smoke.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            explo.Play(true);

            //Create a new enemycannonball whenenever we shoot the enemy cannon
            GameObject cannonBallInst = Instantiate(cannonBall, shotPos.position, shotPos.rotation) as GameObject;

            ballInAction = true;
            cannonBallInst.transform.eulerAngles = new Vector3(0, 0, 0);
            cannonBallRB = cannonBallInst.GetComponent<Rigidbody>();
            cannonBallRB.velocity = shotPos.up * forwardPower;
        }
    }

    void stopShootingEffect()
    {
            explo.Clear();
            explo.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            smoke.Play(true);
    }
}
