/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic to animate the wrong (green) Gem and starts the boss fight
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrongGem : MonoBehaviour
{
    [SerializeField]
    private GameObject skeleton;

    [SerializeField]
    private GameObject cannonRowLeft;

    [SerializeField]
    private GameObject cannonRowRight;

    [SerializeField]
    private GameObject cannonRowFront;

    [SerializeField]
    private AudioSource music;


    bool dir;
    // Start is called before the first frame update
    void Start()
    {
        dir = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (dir)
        {
            var pos = transform.position;
            pos.y += 0.005f;
            transform.position = pos;
            if (transform.position.y > 1.3f)
            {
                dir = false;
            }
        }
        else
        {
            var pos = transform.position;
            pos.y -= 0.005f;
            transform.position = pos;
            if (transform.position.y < 0.7f)
            {
                dir = true;
            }
        }

        transform.Rotate(0.0f, 0.4f, 0.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        //Once user collects green gem, cannons stop and start the boss stage
        cannonRowLeft.GetComponent<ControllingCannons>().enableShooting = false;
        cannonRowRight.GetComponent<ControllingCannons>().enableShooting = false;
        cannonRowFront.GetComponent<ControllingCannons>().enableShooting = false;
        skeleton.SetActive(true);
        music.Stop();
        Destroy(this.gameObject);

    }
}
