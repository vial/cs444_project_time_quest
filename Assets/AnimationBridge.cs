/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains the logic to animate the descending of the cannon

*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBridge : MonoBehaviour
{
    public bool target_one_hit = false;
    public bool target_two_hit = false;

    [SerializeField]
    private AudioSource soundLowerBridge;
    private bool playAudio;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("AnimationForBridge", 0f, 0.0010f);  //1s delay, repeat every 1s
    }

    /// <summary>
	/// This method make the bridge go down whenever the 2 targets are hit
	/// </summary>
    void AnimationForBridge()
    {
        if (target_one_hit && target_two_hit && gameObject.transform.rotation.eulerAngles.x > 100)
        {
            if(!playAudio)
            {
                playAudio=true;
                soundLowerBridge.Play();
            }
            gameObject.transform.Rotate(0.050f, 0, 0);
        }
    }
}
