/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic to make gameObject grabbable
	         and manage interactions with different grab objects
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAnchor : MonoBehaviour
{

	[Header("Grasping Properties")]
	public float graspingRadius = 0.1f;


	[Header("Throwing Force")]
	public float throwingF = 1.0f;

	// Store initial transform parent
	protected Transform initial_transform_parent;
	protected Rigidbody rb;

	protected Quaternion m_anchorOffsetRotation;
	protected Vector3 m_anchorOffsetPosition;

	protected GameObject anchorHand;
	void Awake()
	{
		anchorHand = GameObject.Find("RightControllerAnchor");
		m_anchorOffsetPosition = anchorHand.transform.localPosition;
		m_anchorOffsetRotation = anchorHand.transform.localRotation;
	}
	void Start()
	{
		if (gameObject.name != "pivot_canon" && gameObject.name != "pivot_cart_right" && gameObject.name != "pivot_cart_left")
		{
			initial_transform_parent = transform.parent;
			rb = gameObject.GetComponent<Rigidbody>();
		}
	}


	// Store the hand controller this object will be attached to
	protected HandController hand_controller = null;

	public virtual void attach_to(HandController hand_controller)
	{

		// Store the hand controller in memory
		this.hand_controller = hand_controller;

		rb.isKinematic = true;

		//set trigger

		if (gameObject.name.Contains("cannon ball"))
		{
			var col = gameObject.GetComponent<SphereCollider>();
			col.isTrigger = true;
		}
		else
		{
			var col = gameObject.GetComponent<BoxCollider>();
			col.isTrigger = true;
		}
		// Set the object to be placed in the hand controller referential
		transform.SetParent(hand_controller.transform);
	}

	public virtual void detach_from(HandController hand_controller)
	{
		// Make sure that the right hand controller ask for the release
		if (this.hand_controller != hand_controller) return;

		// Detach the hand controller
		this.hand_controller = null;



		if (gameObject.name.Contains("cannon ball"))
		{
			var col = gameObject.GetComponent<SphereCollider>();
			col.isTrigger = false;
		}
		else
		{
			var col = gameObject.GetComponent<BoxCollider>();
			col.isTrigger = false;

			OVRPose localPose = new OVRPose { position = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch), orientation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch) };
			OVRPose offsetPose = new OVRPose { position = m_anchorOffsetPosition, orientation = m_anchorOffsetRotation };
			localPose = localPose * offsetPose;

			OVRPose trackingSpace = anchorHand.transform.ToOVRPose() * localPose.Inverse();

			rb.velocity = trackingSpace.orientation * OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);

			rb.velocity *= throwingF;
			rb.angularVelocity = trackingSpace.orientation * OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);
		}

		//not Kinematic after grasp + add hand velocity
		rb.isKinematic = false;

		transform.SetParent(initial_transform_parent);
	}

	public virtual bool is_available() { return hand_controller == null; }

	public virtual float get_grasping_radius() { return graspingRadius; }

	//no need upgrade to grasp an object
	public virtual bool can_be_grasped_by(MainPlayerController player) { return true; }
}
