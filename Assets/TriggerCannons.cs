/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains the logic to trigger enemies cannons
           located in the boss's arena, this script must be attached to an object
           with a collider.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCannons : MonoBehaviour
{

    [SerializeField]
    private GameObject cannonRowLeft;

    [SerializeField]
    private GameObject cannonRowRight;

    [SerializeField]
    private GameObject cannonRowFront;

    void OnTriggerEnter(Collider other)
    {
        //to avoid any other object apart from the player triggering the cannons
        if (other.gameObject.name == "OVRPlayerController")
        {
            cannonRowLeft.GetComponent<ControllingCannons>().enableShooting = true;
            cannonRowRight.GetComponent<ControllingCannons>().enableShooting = true;
            cannonRowFront.GetComponent<ControllingCannons>().enableShooting = true;
        }
    }
}
