/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic to make gameObject magnetic grabbable
	         and manage interactions with different grab objects
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticGrabObject : ObjectAnchor
{
    public float velocity_magn;
    private Vector3 previous;
    void Update()
    {
        velocity_magn = ((transform.position - previous).magnitude) / Time.deltaTime;
        previous = transform.position;
    }

}
