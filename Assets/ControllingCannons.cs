/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script works as a field that can be accessed from other
           scripts to allow row cannons to stop/start.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllingCannons : MonoBehaviour
{
    public bool enableShooting;
    // Start is called before the first frame update
    void Start()
    {
        enableShooting = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
