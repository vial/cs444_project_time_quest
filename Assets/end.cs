/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script handles the teleportation and animations to the final zone of the game
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class end : MonoBehaviour
{

    [SerializeField]
    private AudioSource tpSound;

    [SerializeField]
    private Collider TPCollider;

    [SerializeField]
    private AudioSource endSound;

    [SerializeField]
    private AudioSource preEndSound;

    [SerializeField]
    private OVRScreenFade sFade;

    [SerializeField]
    private Transform playerTransform;

    [SerializeField]
    private Transform cameraTransform;

    [SerializeField]
    private Canvas playerHealth;


    /// <summary>
	/// This method is called when the player enter the Teleport.
    /// It ends the actual music, play TP sound, fadeOut effect,
    /// wait a few seconds, teleport player to end zone, fadeIn effect and play final music
	/// </summary>
    IEnumerator OnTriggerEnter(Collider other)
    {
        TPCollider.enabled = false;
        preEndSound.Stop();
        playerHealth.enabled = false;
        tpSound.Play();

        sFade.FadeOut();

        yield return new WaitForSeconds(6);

        playerTransform.position = new Vector3(0f, 10002f, 0f);
        cameraTransform.localPosition = new Vector3(0f, 0f, 0f);

        sFade.FadeIn();

        endSound.Play();

    }
}
