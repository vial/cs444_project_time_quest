/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic for breakable objects with
           cannonballs
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]


public class ExplosiveMaterial : MonoBehaviour
{

    [SerializeField] private GameObject _replacement;
    [SerializeField] private float _breakForce = 2;
    [SerializeField] private float _collisionMultiplier = 100;
    [SerializeField] private bool _broken;
    private GameObject _secondHalfBridge;
    private GameObject _fence;

    // Start is called before the first frame update
    void Start()
    {
        _secondHalfBridge = GameObject.Find("SecondHalfBridge");
        _fence = GameObject.Find("fence_tutorial");
    }
    void Update()
    {
    }


    void OnCollisionEnter(Collision collision) {
        
      if (collision.gameObject.name.Contains("cannon ball"))
      {
            //say to the fence that the targets have been hit
            if (gameObject.name == "target_tutorial_1")
                _fence.GetComponent<FenceTutorial>().waitingT1 = true;
            else if (gameObject.name == "target_tutorial_2")
                _fence.GetComponent<FenceTutorial>().waitingT2 = true;
        if (_broken)
          return;
        if (collision.relativeVelocity.magnitude >= _breakForce) {
            _broken = true;
                //say to the bridge that the targets have been hit
                if (gameObject.name == "right_bridge_target")
                    _secondHalfBridge.GetComponent<AnimationBridge>().target_one_hit = true;
                else if (gameObject.name == "left_bridge_targe")
                    _secondHalfBridge.GetComponent<AnimationBridge>().target_two_hit = true;
        }

           var replacement = Instantiate(_replacement, transform.position, transform.rotation);

            //enable physics if an object was kinematic
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;

            var rbs = replacement.GetComponentsInChildren<Rigidbody>();
            Destroy(gameObject);

            //go through the replacement and add a force for each piece fractured to simulate explosion
            foreach (var rb in rbs) {
                rb.useGravity = true;
                rb.isKinematic = false;
            rb.AddExplosionForce(collision.relativeVelocity.magnitude * _collisionMultiplier,collision.contacts[0].point,2);
        }

      }
    }

}
