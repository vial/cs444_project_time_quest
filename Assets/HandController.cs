/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic to check for the grabbing of
					simple/magnetic objects. In addition, it manages states of some GameObject objects
					such as the cannon and teleporting bomb.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour
{

	// Store the hand type to know which button should be pressed
	public enum HandType : int { LeftHand, RightHand };
	[Header("Hand Properties")]
	public HandType handType;


	// Store the player controller to forward it to the object
	[Header("Player Controller")]
	public MainPlayerController playerController;

	public LineRenderer line;

	private bool isHandlingObject;

	// Store all gameobjects containing an Anchor
	// N.B. This list is static as it is the same list for all hands controller
	// thus there is no need to duplicate it for each instance
	static protected ObjectAnchor[] anchors_in_the_scene;
	void Start()
	{
		// Prevent multiple fetch
		if (anchors_in_the_scene == null) anchors_in_the_scene = GameObject.FindObjectsOfType<ObjectAnchor>();

		isHandlingObject = false;
		Vector3[] startLinePos = new Vector3[2] { Vector3.zero, Vector3.zero };
		line.SetPositions(startLinePos);
		line.enabled = false;
	}


	// This method checks that the right hand is closed
	protected bool is_right_hand_closed()
	{
		// Check that the A button is pressed
		if (handType == HandType.RightHand) return OVRInput.Get(OVRInput.Button.One);
		else return false;
	}


	// Automatically called at each frame
	void Update()
	{
		laser_grab_behavior();
		handle_controller_behavior();
	}


	/// <summary>
	/// This method helps to detect when the bomb has been grapped by the player
	/// </summary>
	protected void check_grapping_bomb()
	{
		Debug.LogWarningFormat("{0}", object_grasped.name);
		if (object_grasped.name.Contains("grenade"))
		{
			object_grasped.GetComponent<TeleportBomb>().holdedByPlayer = true;
			object_grasped.GetComponent<TeleportBomb>().nameOfBomb = "";
		}

	}

	/// <summary>
	/// This method handles the triggering of the bomb from the moment it leave its hands
	/// </summary>
	protected void check_throwing_bomb()
	{
		if (object_grasped.name.Contains("grenade"))
		{
			if (object_grasped.GetComponent<TeleportBomb>().holdedByPlayer)
			{
				object_grasped.GetComponent<TeleportBomb>().fired = true;
				object_grasped.GetComponent<TeleportBomb>().nameOfBomb = object_grasped.name;
			}

		}

	}

	protected void handle_cart_canon(ObjectAnchor cart)
    {
		//allow changes in x and y position by the dragging of hand
		cart.transform.position = new Vector3(this.transform.position.x, cart.transform.position.y, this.transform.position.z);
    }


	protected void handle_canon_angle(ObjectAnchor canon)
	{
		canon.transform.eulerAngles = new Vector3(this.transform.rotation.eulerAngles.x, canon.transform.rotation.eulerAngles.y, canon.transform.rotation.eulerAngles.z);
	}

	// Store the previous state of triggers to detect edges
	protected bool is_hand_closed_previous_frame = false;

	// Store the object atached to this hand
	protected ObjectAnchor object_grasped = null;

	/// <summary>
	/// This method handles the laser grab (grab a magnetic object that is focused by a ray)
	/// </summary>
	protected void laser_grab_behavior()
	{
		if (is_right_hand_closed())
		{
			//If hand is not handling any object, enable ray
			if (!isHandlingObject)
			{
				line.enabled = true;

				RaycastHit hit;
				Ray lineIsOut = new Ray(this.transform.position, this.transform.forward);
				Vector3 endPosition = this.transform.position + (10 * this.transform.forward);

				//check if ray hits an object
				if (Physics.Raycast(lineIsOut, out hit))
				{
					endPosition = hit.point;
					GameObject objectHit = hit.collider.gameObject;

					//can't take ennemy's sword
					if(objectHit.name != "Axe_DS_Ennemy" && objectHit.name != "SWORD")
                    {
						//check if magnetic object and if index trigger is pressed
						if (objectHit.GetComponents<MagneticGrabObject>().Length != 0 && OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) > 0.5)
						{
							//align object with the hand
							objectHit.transform.position = this.transform.position;
							objectHit.transform.rotation = this.transform.rotation;
							objectHit.transform.Rotate(-35f, 0, 90f);

							// Store in memory the object grasped
							object_grasped = objectHit.GetComponent<ObjectAnchor>();

							// Log the grasp
							Debug.LogWarningFormat("{0} grasped {1}", this.transform.parent.name, object_grasped.name);


							check_grapping_bomb();

							// Grab this object
							object_grasped.attach_to(this);

							//hand is handling an object
							isHandlingObject = true;

							//deactivate ray
							line.enabled = false;

						}
					}

				}
				line.SetPosition(0, this.transform.position);
				line.SetPosition(1, endPosition);
			}


		}
		else
		{
			line.enabled = false;
		}
	}

	/// <summary>
	/// This method handles the linking of object anchors to this hand controller
	/// </summary>
	protected void handle_controller_behavior()
	{

		// Check if there is a change in the grasping state (i.e. an edge) otherwise do nothing
		bool hand_closed = is_right_hand_closed();
		if (hand_closed == is_hand_closed_previous_frame) return;
		is_hand_closed_previous_frame = hand_closed;



		//==============================================//
		// Define the behavior when the hand get closed //
		//==============================================//
		if (hand_closed)
		{

			// Log hand action detection
			Debug.LogWarningFormat("{0} get closed", this.transform.parent.name);

			// Determine which object available is the closest from the right hand
			int best_object_id = -1;
			float best_object_distance = float.MaxValue;
			float oject_distance;

			// Iterate over objects to determine if we can interact with it
			for (int i = 0; i < anchors_in_the_scene.Length; i++)
			{

				// Skip object not available
				if (!anchors_in_the_scene[i].is_available()) continue;
				Debug.LogWarningFormat("{0} no anchor in scene", 1);
				// Skip object requiring special upgrades
				if (!anchors_in_the_scene[i].can_be_grasped_by(playerController)) continue;

				// Compute the distance to the object
				oject_distance = Vector3.Distance(this.transform.position, anchors_in_the_scene[i].transform.position);

				// Keep in memory the closest object
				if (oject_distance < best_object_distance && oject_distance <= anchors_in_the_scene[i].get_grasping_radius())
				{
					best_object_id = i;
					best_object_distance = oject_distance;
				}
			}

			// If the best object is in range grab it
			if (best_object_id != -1)
			{
				//hand is handling an object
				isHandlingObject = true;

				//deactivate ray
				line.enabled = false;

				// Store in memory the object grasped
				object_grasped = anchors_in_the_scene[best_object_id];

				// Log the grasp
				Debug.LogWarningFormat("{0} grasped {1}", this.transform.parent.name, object_grasped.name);


				// Check if handle the cart or cannon
				if (object_grasped.name == "pivot_canon")
				{
					object_grasped.transform.parent.gameObject.GetComponent<Canon>().moveCanon = true;
				}
				else if (object_grasped.name == "pivot_cart_left" || object_grasped.name == "pivot_cart_right")
				{
					object_grasped.transform.parent.GetComponent<Canon>().moveCart = true;
					object_grasped.transform.parent.GetComponent<Canon>().centerPlayer = false;
				}
				else
				{
					// Grab this object
					object_grasped.attach_to(this);

					check_grapping_bomb();

					//align the object with the hand
					object_grasped.transform.position = this.transform.position;
					object_grasped.transform.rotation = this.transform.rotation;
					object_grasped.transform.Rotate(-35f, 0, 90f);
				}


			}


			//==============================================//
			// Define the behavior when the hand get opened //
			//==============================================//
		}
		else if (object_grasped != null)
		{

			//released the object
			isHandlingObject = false;

			// Log the release
			Debug.LogWarningFormat("{0} released {1}", this.transform.parent.name, object_grasped.name);


			// Check if handle the cart or cannon
			if (object_grasped.name == "pivot_canon")
			{
				object_grasped.transform.parent.GetComponent<Canon>().moveCanon = false;

			}
			else if (object_grasped.name == "pivot_cart_left" || object_grasped.name == "pivot_cart_right")
			{
				object_grasped.transform.parent.GetComponent<Canon>().moveCart = false;
				object_grasped.transform.parent.GetComponent<Canon>().centerPlayer = false;
				object_grasped.transform.parent.GetComponent<Canon>().restartPhysics = true;
			}
			else
			{

				check_throwing_bomb();
				// Release the object
				object_grasped.detach_from(this);
			}

		}
	}
}
