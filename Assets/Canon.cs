/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic for player's cannon
           loading, shooting, tilting, movement and trajectoring

  Code for trajectory cannon ball from: https://github.com/akonig513/3DProjectileTutorial/blob/master/3D%20Projectile%20Tutorial/Assets/DrawProjection.cs
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(MeshCollider))]

public class Canon : MonoBehaviour
{
    private bool alreadyLoaded;

    public GameObject cannonBall;
    public GameObject PlayerRef;
    Rigidbody cannonBallRB;
    public Transform shotPos;
    public GameObject explosion;
    public GameObject canonMesh;
    public float forwardPower;
    public GameObject HandRef;
    public bool moveCart;
    public bool moveCanon;
    private float originalPosPivot;
    public float originalAngle;
    public bool centerPlayer;
    public float degrees;
    public float maximumHeight;
    public GameObject posRelocationPlayer;
    public bool restartPhysics;
    public Transform originalParentOfPivot;
    private ParticleSystem explo;
    private ParticleSystem smoke;
    private bool projectileShot;

    [Header("Shooting delay")]
    public float shootingTimeSec = 3.5f;

    [SerializeField]
    private AudioSource shootSound;
    private GameObject collideCannonBall;

    private LineRenderer lineRenderer;

    // The physics layers that will cause the line to stop being drawn
    public LayerMask CollidableLayers;

    public int numPoints = 50;

    // distance between those points on the line
    public float timeBetweenPoints = 0.1f;

    // Store initial transform parent
    protected Transform initial_transform_parent;

    // Start is called before the first frame update
    void Start()
    {

        lineRenderer = GetComponent<LineRenderer>();
        centerPlayer = false;
        initial_transform_parent = transform.parent;
        alreadyLoaded = false;

        //The following is a relation between tilting angles with height already computed
        //----------------------------------------------------------------------
        maximumHeight = 0.22f;
        degrees = 90.0f;
        //----------------------------------------------------------------------
        explo = this.transform.Find("Cube_001").gameObject.transform.Find("shootPosition").gameObject.transform.Find("SmallExplosionEffect").gameObject.GetComponent<ParticleSystem>();
        explo.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        smoke = this.transform.Find("SmokeEffect").gameObject.GetComponent<ParticleSystem>();
        smoke.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        canonMesh = transform.GetChild(1).gameObject;
        HandRef = GameObject.Find("RightControllerAnchor");
        PlayerRef = GameObject.Find("OVRPlayerController");
        originalParentOfPivot = this.transform.Find("pivot_canon").gameObject.transform.parent;
        originalPosPivot = this.transform.Find("pivot_canon").gameObject.transform.position.y;
        originalAngle = this.transform.localRotation.eulerAngles.z;
        posRelocationPlayer = this.transform.Find("positionPushingCanon").gameObject;
    }
    public void attachToPull()
    {
        this.transform.Find("pivot_canon").gameObject.transform.SetParent(this.transform.Find("Cube_002").gameObject.transform);
    }
    public void deattachToPull()
    {
        this.transform.Find("Cube_002").gameObject.transform.Find("pivot_canon").gameObject.transform.SetParent(originalParentOfPivot);
    }
    void Update()
    {

        if(projectileShot)
        {
            Invoke("stopShootingEffect", 1.7f);
            projectileShot = false;
        }
     if (restartPhysics && !moveCanon)
        {
            transform.SetParent(initial_transform_parent);
            restartPhysics = false;
        }

      if(moveCart)
      {
        if(!centerPlayer)
        {
                PlayerRef.transform.position = new Vector3(posRelocationPlayer.transform.position.x, PlayerRef.transform.position.y, posRelocationPlayer.transform.position.z);
                Vector3 newRotation = new Vector3(0, this.transform.eulerAngles.y+180, 0);
                PlayerRef.transform.eulerAngles = newRotation;
                centerPlayer = true;
                //set the cannon tto the player to move and orient the cannon
                transform.SetParent(PlayerRef.transform);
            }

            //lock player's rotation in x and z
            PlayerRef.transform.localEulerAngles = new Vector3(0, PlayerRef.transform.localRotation.eulerAngles.y, 0);
        }
      else if(moveCanon)
      {

        /*An offset of height is computed by subtracting  the right hand height with the original height of the pivot,
         then by using the precomputed relation height of pivot and tilting angle,
         a local angle for middle handle and barrel can be applied,
         the pivot is attached to the middle handle whenever a tilting is executed and then detached,
        this allows the displacement of the pivot to the tip of the middle handle
        */

            float positionOfHand = HandRef.transform.position.y;
            float newMiddlePosition =  positionOfHand - originalPosPivot;
            float angleX = 0f;
            angleX = -1 * (float)Math.Round(newMiddlePosition,2) * degrees / (maximumHeight);
            angleX = Mathf.Clamp(angleX, -90, 90);
            var angles = new Vector3(angleX, transform.GetChild(1).gameObject.transform.localRotation.eulerAngles.y, transform.GetChild(1).gameObject.transform.localRotation.eulerAngles.z);
            attachToPull();
            transform.GetChild(1).gameObject.transform.localEulerAngles = angles;
            this.transform.Find("Cube_001").gameObject.transform.localEulerAngles = angles;
            deattachToPull();

        }

        if (moveCanon || moveCart || alreadyLoaded)
        {
            //allow to produce trajectory
            //calculate points over a time by using the formula
            // y = y0+v0.t + gt^2
            lineRenderer.positionCount = (int)numPoints;
            List<Vector3> points = new List<Vector3>();
            Vector3 startingPosition = shotPos.position;
            Vector3 startingVelocity = shotPos.up * forwardPower;
            for (float t = 0; t < numPoints; t += timeBetweenPoints)
            {
                Vector3 newPoint = startingPosition + t * startingVelocity;
                newPoint.y = startingPosition.y + startingVelocity.y * t + Physics.gravity.y / 2f * t * t;
                points.Add(newPoint);

                if (Physics.OverlapSphere(newPoint, 0.01f, CollidableLayers).Length > 0)
                {
                    lineRenderer.positionCount = points.Count;
                    break;
                }
            }

            lineRenderer.SetPositions(points.ToArray());

        }
        else
        {
            //clean the trajectory
            List<Vector3> points = new List<Vector3>();
            lineRenderer.positionCount = points.Count;
            lineRenderer.SetPositions(points.ToArray());
        }

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (!alreadyLoaded && collision.gameObject.name.Contains("cannon ball"))
        {
            alreadyLoaded = true;
            smoke.Play(true);
            Invoke("shootBall", shootingTimeSec);
            collideCannonBall = collision.gameObject;
            collideCannonBall.SetActive(false);
        }
    }
    private void shootBall()
    {
        shootSound.Play();
        smoke.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        explo.Play(true);
        projectileShot = true;
        collideCannonBall.SetActive(true);
        collideCannonBall.transform.position = shotPos.position;
        collideCannonBall.transform.eulerAngles = new Vector3(0, 0, 0);
        cannonBallRB = collideCannonBall.GetComponent<Rigidbody>();
        cannonBallRB.velocity = shotPos.up * forwardPower;
        alreadyLoaded = false;
    }

     void stopShootingEffect()
    {
        explo.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }
}
