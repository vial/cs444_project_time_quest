/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic to handle  the destruction of enemy ballcannons 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class clearCannonBall : MonoBehaviour
{
    protected bool enableDestruction;

      [Header("Delay_to_clear_canonball")]
      public float delayForClearingCannonBall = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        enableDestruction = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        //Avoid invoking twice the destroy function by checking if the destruction
        //has already been issued
        if (!enableDestruction)
        {
            Invoke("destroyCannonBall", delayForClearingCannonBall);
            enableDestruction = true;
        }
    }
    void destroyCannonBall()
    {
        Destroy(this.gameObject);
    }

}
