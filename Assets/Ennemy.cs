/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains the set up for all enemies in the game (manages health points)
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemy : MonoBehaviour
{
    [SerializeField]
    private int maxHealth = 100;

    protected int currentHealth;
    public event Action<float> OnHealthPctChanged = delegate { };

    protected virtual void Start()
    {
        currentHealth = maxHealth;
    }

    /// <summary>
	/// This method modifies the health bar by a given amount
	/// </summary>
    public void ModifyHealth(int amount)
    {
        currentHealth += amount;

        float currentHealthPct = (float)currentHealth / (float)maxHealth;
        OnHealthPctChanged(currentHealthPct);
    }
}
