/*
  Authors: Nicolas Vial
  Date: 25/05/2022
  Summary: The following script contains logic to animate the Gem and enables de teleport to end of game
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGem : MonoBehaviour
{

    [SerializeField]
    private GameObject teleport;

    [SerializeField]
    private AudioSource preEndSound;

    private bool dir;
    private int count;
    // Start is called before the first frame update
    void Start()
    {
        dir = true;
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Animation of gem
        if (dir)
        {
            var pos = transform.position;
            pos.y += 0.005f;
            transform.position = pos;
            if (transform.position.y > 2f)
            {
                dir = false;
            }
        }
        else
        {
            var pos = transform.position;
            pos.y -= 0.005f;
            transform.position = pos;
            if (transform.position.y < 1.2f)
            {
                dir = true;
            }
        }

        transform.Rotate(0.0f, 0.4f, 0.0f);

        //wait a bit before playing the pre-end music so that victory music has time to finish
        if(count == 200)
        {
            preEndSound.Play();
        }
        count += 1;

    }

    void OnTriggerEnter(Collider other)
    {
        //when player takes the gem, teleport becomes active and gem is destroyed
        teleport.SetActive(true);
        Destroy(this.gameObject);

    }
}
