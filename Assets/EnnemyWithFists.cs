/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains the logic for enemies attacking with fists
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyWithFists : Ennemy
{
    [SerializeField]
    private Collider takeDmgCollider;

    [SerializeField]
    private Collider doDmgColliderL;

    [SerializeField]
    private Transform target;

    [SerializeField]
    public Animator anim;

    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    private AudioSource dmgTaken1;

    [SerializeField]
    private AudioSource dmgTaken2;

    [SerializeField]
    private AudioSource dmgTaken3;

    [SerializeField]
    private AudioSource dmgTaken4;

    [SerializeField]
    private AudioSource dmgTaken5;

    [SerializeField]
    private AudioSource deathSound;

    private int checkHit;
    private int punchFrameCount;
    private bool dead;
    private bool punching;
    private int framesB4NextAttack;
    private float relativeDistance;
    private List<AudioSource> soundsList = new List<AudioSource>();
    private System.Random random = new System.Random();

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        checkHit = 0;
        dead = false;
        punching = false;
        framesB4NextAttack = 300;
        punchFrameCount = 0;
        soundsList.Add(dmgTaken1);
        soundsList.Add(dmgTaken2);
        soundsList.Add(dmgTaken3);
        soundsList.Add(dmgTaken4);
        soundsList.Add(dmgTaken5);
        doDmgColliderL.enabled = false;
        relativeDistance = Vector3.Distance(target.position, transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        //If dead : deactivate everything + death animation
        if (dead)
        {
            anim.SetBool("RunForward", false);
            anim.SetBool("Death", true);
            canvas.gameObject.SetActive(false);
        }
        else
        {
            //update distance between player and ennemy
            relativeDistance = Vector3.Distance(target.position, transform.position);

            //Wait 1 frame before setting GetHit animation to false
            if (anim.GetBool("GetHit"))
            {
                if (checkHit == 1)
                {
                    anim.SetBool("GetHit", false);
                    checkHit = 0;
                }
                else
                {
                    checkHit = 1;
                }
            }

            //when close enough to player, attack with fist
            if (relativeDistance < 1.9f)
            {
                //stop running
                anim.SetBool("RunForward", false);

                //punch when target is close enough and if ennemy can attack
                if (!punching && Time.frameCount - punchFrameCount > framesB4NextAttack && !dead)
                {
                    doDmgColliderL.enabled = true;
                    anim.SetBool("PunchRight", true);
                    punchFrameCount = Time.frameCount;
                    punching = true;
                }
            }
            //when not close enough to player and not too far away, run to player
            else
            {
                if (relativeDistance < 10f){
                    //start running
                    anim.SetBool("RunForward", true);
                    transform.position += 0.008f * new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z);
                }
            }

            //Wait end of animation to desable fist hitbox
            if (Time.frameCount - punchFrameCount == 75)
            {
                doDmgColliderL.enabled = false;
                anim.SetBool("PunchRight", false);
                punching = false;
            }
        }



    }

    void OnTriggerEnter(Collider other)
    {
        //When hit by player, take dmg + sound + animation
        //dmg taken depends on the weapon used by the player

        if (other.gameObject.name == "Sword_OH")
        {
            MagneticGrabObject m = other.gameObject.GetComponent<MagneticGrabObject>();
            base.ModifyHealth(-(int)(m.velocity_magn * 2));
            //if still alive after getting hit play hit sound and otherwise set ennemy dead
            if (currentHealth > 0)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                if (dead == false)
                {
                    deathSound.Play();
                    takeDmgCollider.enabled = false;
                    doDmgColliderL.enabled = false;
                }
                dead = true;
            }

            anim.SetBool("GetHit", true);
        }

        if (other.gameObject.name == "Ally_Sword")
        {
            MagneticGrabObject m = other.gameObject.GetComponent<MagneticGrabObject>();
            base.ModifyHealth(-(int)(m.velocity_magn * 3.5));
            if (currentHealth > 0)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                if (dead == false)
                {
                    deathSound.Play();
                    takeDmgCollider.enabled = false;
                    doDmgColliderL.enabled = false;
                }
                dead = true;
            }

            anim.SetBool("GetHit", true);
        }
    }

    private void LateUpdate()
    {
        //make ennemy look at player when alive and close enough
        if (!dead && relativeDistance < 10f)
        {
            float x = transform.localEulerAngles.x;
            float z = transform.localEulerAngles.z;
            transform.LookAt(Camera.main.transform);
            float new_x = transform.localEulerAngles.x;
            float new_z = transform.localEulerAngles.z;
            transform.Rotate(-new_x + x, 0, -new_z + z);
        }
    }
}
