/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains logic for managing a health bar of ennemies
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Image foregroundImage;

    [SerializeField]
    private float updateSpeedSeconds = 0.5f;

    private void Awake()
    {
        GetComponentInParent<Ennemy>().OnHealthPctChanged += HandleHealthChanged;
    }

    private void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPct(pct));
    }

    private IEnumerator ChangeToPct(float pct)
    {
        float preChange = foregroundImage.fillAmount;
        float elapsed = 0f;

        //animation of healthbar going down
        while(elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime;
            foregroundImage.fillAmount = Mathf.Lerp(preChange, pct, elapsed / updateSpeedSeconds);
            yield return null;
        }

        foregroundImage.fillAmount = pct;
    }

    private void LateUpdate()
    {
        //make healthbar look at player
        transform.LookAt(Camera.main.transform);
        transform.Rotate(0, 180, 0);
    }
}
