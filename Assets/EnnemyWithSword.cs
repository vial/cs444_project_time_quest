/*
  Authors: Nicolas Vial, Marcel Moran Calderon
  Date: 25/05/2022
  Summary: The following script contains the logic for enemies attacking with swords
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyWithSword : Ennemy
{
    [SerializeField]
    private float runDistance;

    [SerializeField]
    private float hitDistance;

    [SerializeField]
    private Collider takeDmgCollider;

    [SerializeField]
    private Transform target;

    [SerializeField]
    private Transform sword;

    [SerializeField]
    private Collider swordCollider;

    [SerializeField]
    private Rigidbody swordRigidbody;

    [SerializeField]
    public Animator anim;

    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    private AudioSource dmgTaken1;

    [SerializeField]
    private AudioSource dmgTaken2;

    [SerializeField]
    private AudioSource dmgTaken3;

    [SerializeField]
    private AudioSource dmgTaken4;

    [SerializeField]
    private AudioSource dmgTaken5;

    [SerializeField]
    private AudioSource deathSound;

    [SerializeField]
    private AudioSource boss_start;

    [SerializeField]
    private AudioSource boss_death;

    [SerializeField]
    private AudioSource bossMusic;

    [SerializeField]
    private AudioSource winBossMusic;

    [SerializeField]
    private GameObject final_gem;

    [SerializeField]
    private GameObject final_wall;

    private GameObject effectsFire;
    private int checkHit;
    private int attackFrameCount;
    private bool dead;
    private bool attacking;
    private int framesB4NextAttack;
    private float relativeDistance;
    private List<AudioSource> soundsList = new List<AudioSource>();
    private System.Random random = new System.Random();

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        swordCollider.enabled = false;
        swordRigidbody.isKinematic = true;
        sword.gameObject.GetComponent<MagneticGrabObject>().enabled = false;
        sword.gameObject.GetComponent<ObjectAnchor>().enabled = false;

        effectsFire = GameObject.Find("fireForBossXiLef");
        checkHit = 0;
        dead = false;
        attacking = false;
        framesB4NextAttack = 400;
        attackFrameCount = 0;
        soundsList.Add(dmgTaken1);
        soundsList.Add(dmgTaken2);
        soundsList.Add(dmgTaken3);
        soundsList.Add(dmgTaken4);
        soundsList.Add(dmgTaken5);
        relativeDistance = Vector3.Distance(target.position, transform.position);

        //if final boss, change music
        if(this.gameObject.name == "SKELETON")
        {
            boss_start.Play();
            bossMusic.Play();
        }
    }


    // Update is called once per frame
    void Update()
    {
        //If dead : deactivate everything + death animation
        if (dead)
        {
            anim.SetBool("RunForward", false);
            anim.SetBool("Death", true);
        }
        else
        {
            //update distance between player and ennemy
            relativeDistance = Vector3.Distance(target.position, transform.position);

            //Wait 1 frame before setting GetHit animation to false
            if (anim.GetBool("GetHit"))
            {
                if (checkHit == 1)
                {
                    anim.SetBool("GetHit", false);
                    checkHit = 0;
                }
                else
                {
                    checkHit = 1;
                }
            }

            //when close enough to player, attack with sword
            if (relativeDistance < hitDistance)
            {
                //stop running
                anim.SetBool("RunForward", false);

                //attack when target is close enough and if ennemy can attack
                if (!attacking && Time.frameCount - attackFrameCount > framesB4NextAttack && !dead)
                {
                    swordCollider.enabled = true;
                    anim.SetBool("AxeAttack", true);
                    attackFrameCount = Time.frameCount;
                    attacking = true;
                }
            }
            //when not close enough to player and not too far away, run to player
            else
            {
                if (relativeDistance < runDistance)
                {
                    //start running
                    anim.SetBool("RunForward", true);
                    transform.position += 0.005f * new Vector3(target.position.x - transform.position.x, 0, target.position.z - transform.position.z);
                }
            }

            //Wait end of attack animation
            if (Time.frameCount - attackFrameCount == 100)
            {
                swordCollider.enabled = false;
                anim.SetBool("AxeAttack", false);
                attacking = false;
            }
        }



    }

    //When hit by player, take dmg + sound + animation
    void OnTriggerEnter(Collider other)
    {
        //When hit by player, take dmg + sound + animation
        //dmg taken depends on the weapon used by the player

        if (other.gameObject.name == "Sword_OH")
        {
            MagneticGrabObject m = other.gameObject.GetComponent<MagneticGrabObject>();
            base.ModifyHealth(-(int)(m.velocity_magn*2));
            if (currentHealth > 0)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                if (dead == false)
                {
                    if (this.gameObject.name == "SKELETON")
                    {
                        boss_death.Play();
                        bossMusic.Stop();
                        final_gem.SetActive(true);
                        final_wall.SetActive(true);
                        winBossMusic.Play();
                    }
                    else
                    {
                        deathSound.Play();
                    }

                    takeDmgCollider.enabled = false;
                    canvas.gameObject.SetActive(false);
                    swordRigidbody.isKinematic = false;
                    swordCollider.enabled = true;
                    sword.gameObject.GetComponent<MagneticGrabObject>().enabled = true;
                    sword.gameObject.GetComponent<ObjectAnchor>().enabled = true;
                    if (sword.gameObject.name == "Axe_DS_Ennemy")
                    {
                        sword.gameObject.name = "Ally_Sword";
                    }
                    if (sword.gameObject.name == "SWORD")
                    {
                        sword.gameObject.name = "Ally_Big_Sword";
                    }
                    sword.transform.parent = null;
                }
                dead = true;
            }

            anim.SetBool("GetHit", true);

        }
        if (other.gameObject.name == "Ally_Sword")
        {
            MagneticGrabObject m = other.gameObject.GetComponent<MagneticGrabObject>();
            base.ModifyHealth(-(int)(m.velocity_magn*3.5));
            if (currentHealth > 0)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                if (dead == false)
                {
                    if (this.gameObject.name == "SKELETON")
                    {
                        boss_death.Play();
                        bossMusic.Stop();
                        final_gem.SetActive(true);
                        final_wall.SetActive(true);
                        winBossMusic.Play();
                    }
                    else
                    {
                        deathSound.Play();
                    }
                    takeDmgCollider.enabled = false;
                    canvas.gameObject.SetActive(false);
                    swordRigidbody.isKinematic = false;
                    swordCollider.enabled = true;
                    sword.gameObject.GetComponent<MagneticGrabObject>().enabled = true;
                    sword.gameObject.GetComponent<ObjectAnchor>().enabled = true;
                    if(sword.gameObject.name == "Axe_DS_Ennemy")
                    {
                        sword.gameObject.name = "Ally_Sword";
                    }
                    if (sword.gameObject.name == "SWORD")
                    {
                        sword.gameObject.name = "Ally_Big_Sword";
                    }
                    sword.transform.parent = null;
                }
                dead = true;
            }

            anim.SetBool("GetHit", true);

        }
        if (other.gameObject.name == "Ally_Big_Sword")
        {
            MagneticGrabObject m = other.gameObject.GetComponent<MagneticGrabObject>();
            base.ModifyHealth(-(int)(m.velocity_magn * 1000));
            if (currentHealth > 0)
            {
                int index = random.Next(soundsList.Count);
                soundsList[index].Play();
            }
            else
            {
                if (dead == false)
                {
                    if (this.gameObject.name == "SKELETON")
                    {
                        boss_death.Play();
                        bossMusic.Stop();
                        final_gem.SetActive(true);
                        final_wall.SetActive(true);
                        winBossMusic.Play();
                    }
                    else
                    {
                        deathSound.Play();
                    }
                    takeDmgCollider.enabled = false;
                    canvas.gameObject.SetActive(false);
                    swordRigidbody.isKinematic = false;
                    swordCollider.enabled = true;
                    sword.gameObject.GetComponent<MagneticGrabObject>().enabled = true;
                    sword.gameObject.GetComponent<ObjectAnchor>().enabled = true;
                    if (sword.gameObject.name == "Axe_DS_Ennemy")
                    {
                        sword.gameObject.name = "Ally_Sword";
                    }
                    if (sword.gameObject.name == "SWORD")
                    {
                        sword.gameObject.name = "Ally_Big_Sword";
                    }
                    sword.transform.parent = null;
                }
                dead = true;
            }

            anim.SetBool("GetHit", true);

        }
    }

    private void LateUpdate()
    {
        //make ennemy look at player when alive
        if (!dead && relativeDistance < runDistance)
        {
            float x = transform.localEulerAngles.x;
            float z = transform.localEulerAngles.z;
            transform.LookAt(Camera.main.transform);
            float new_x = transform.localEulerAngles.x;
            float new_z = transform.localEulerAngles.z;
            transform.Rotate(-new_x + x, 0, -new_z + z);
        }
    }
}
